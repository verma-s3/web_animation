(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"timeline_control_HTML5 Canvas_atlas_", frames: [[480,386,400,85],[0,209,478,80],[0,291,478,80],[0,127,478,80],[0,373,478,80],[480,222,478,80],[0,0,496,125],[498,0,230,220],[0,455,400,85],[402,473,400,85],[730,0,220,220],[350,560,346,80],[0,542,348,82],[0,626,346,80],[480,304,478,80]]}
];


// symbols:



(lib.CachedTexturedBitmap_1 = function() {
	this.initialize(ss["timeline_control_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_10 = function() {
	this.initialize(ss["timeline_control_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_11 = function() {
	this.initialize(ss["timeline_control_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_13 = function() {
	this.initialize(ss["timeline_control_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_14 = function() {
	this.initialize(ss["timeline_control_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_15 = function() {
	this.initialize(ss["timeline_control_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_16 = function() {
	this.initialize(ss["timeline_control_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_17 = function() {
	this.initialize(ss["timeline_control_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_2 = function() {
	this.initialize(ss["timeline_control_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_3 = function() {
	this.initialize(ss["timeline_control_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_4 = function() {
	this.initialize(ss["timeline_control_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_6 = function() {
	this.initialize(ss["timeline_control_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_7 = function() {
	this.initialize(ss["timeline_control_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_8 = function() {
	this.initialize(ss["timeline_control_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_9 = function() {
	this.initialize(ss["timeline_control_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.star = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_17();
	this.instance.parent = this;
	this.instance.setTransform(-0.5,-0.5,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-0.5,115,110);


(lib.sect2_butt = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_13();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.instance_1 = new lib.CachedTexturedBitmap_14();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,0,0.5,0.5);

	this.instance_2 = new lib.CachedTexturedBitmap_15();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,0,0.5,0.5);

	this.instance_3 = new lib.CachedTexturedBitmap_16();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-6.45,-9.25,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.4,-9.2,248,62.5);


(lib.sect1_butt = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_9();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.instance_1 = new lib.CachedTexturedBitmap_10();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,0,0.5,0.5);

	this.instance_2 = new lib.CachedTexturedBitmap_11();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,0,0.5,0.5);

	this.instance_3 = new lib.CachedTexturedBitmap_16();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-6.45,-9.25,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.4,-9.2,248,62.5);


(lib.main_menu = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_6();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.instance_1 = new lib.CachedTexturedBitmap_7();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.5,-0.5,0.5,0.5);

	this.instance_2 = new lib.CachedTexturedBitmap_8();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).to({state:[{t:this.instance_1},{t:this.instance_2}]},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-0.5,174,41);


(lib.green_ball = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_4();
	this.instance.parent = this;
	this.instance.setTransform(-0.5,-0.5,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-0.5,110,110);


// stage content:
(lib.timeline_control_HTML5Canvas = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{menu:0,section_1:29,section_2:59,end:89});

	// timeline functions:
	this.frame_0 = function() {
		//// stop the timeline 
		//stop();
		///* Click to Go to Frame and Play
		//Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and continues playback from that frame.
		//Can be used on the main timeline or on movie clip timelines.
		//
		//Instructions:
		//1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		//*/
		//
		//sect1_butt.addEventListener(MouseEvent.CLICK, fl_ClickToGoToAndPlayFromFrame_3);
		//
		//function fl_ClickToGoToAndPlayFromFrame_3(event:MouseEvent):void
		//{
		//	gotoAndPlay('section_1');
		//}
		//
		///* Click to Go to Frame and Play
		//Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and continues playback from that frame.
		//Can be used on the main timeline or on movie clip timelines.
		//
		//Instructions:
		//1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		//*/
		//
		//sect2_butt.addEventListener(MouseEvent.CLICK, fl_ClickToGoToAndPlayFromFrame_4);
		//
		//function fl_ClickToGoToAndPlayFromFrame_4(event:MouseEvent):void
		//{
		//	gotoAndPlay('section_2');
		//}
		//
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.sect1_butt.addEventListener("click", fl_ClickToGoToAndStopAtFrame.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame()
		{
			this.gotoAndPlay('section_2');
		}
	}
	this.frame_54 = function() {
		//stop();
		///* Click to Go to Frame and Stop
		//Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		//Can be used on the main timeline or on movie clip timelines.
		//
		//Instructions:
		//1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		//*/
		//
		//main_menu_1.addEventListener(MouseEvent.CLICK, fl_ClickToGoToAndStopAtFrame);
		//
		//function fl_ClickToGoToAndStopAtFrame(event:MouseEvent):void
		//{
		//	gotoAndStop('menu');
		//}
		//
		//
		//
	}
	this.frame_89 = function() {
		//stop();
		///* Click to Go to Frame and Stop
		//Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		//Can be used on the main timeline or on movie clip timelines.
		//
		//Instructions:
		//1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		//*/
		//
		//main_menu_1.addEventListener(MouseEvent.CLICK, fl_ClickToGoToAndStopAtFrame_3);
		//
		//function fl_ClickToGoToAndStopAtFrame_3(event:MouseEvent):void
		//{
		//	gotoAndStop('menu');
		//}
		//
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(54).call(this.frame_54).wait(35).call(this.frame_89).wait(1));

	// Buttons
	this.sect2_butt = new lib.sect2_butt();
	this.sect2_butt.name = "sect2_butt";
	this.sect2_butt.parent = this;
	this.sect2_butt.setTransform(29.05,155.2);
	new cjs.ButtonHelper(this.sect2_butt, 0, 1, 2, false, new lib.sect2_butt(), 3);

	this.sect1_butt = new lib.sect1_butt();
	this.sect1_butt.name = "sect1_butt";
	this.sect1_butt.parent = this;
	this.sect1_butt.setTransform(139.65,105.35,1,1,0,0,0,110.6,21.3);
	new cjs.ButtonHelper(this.sect1_butt, 0, 1, 2, false, new lib.sect1_butt(), 3);

	this.main_menu_1 = new lib.main_menu();
	this.main_menu_1.name = "main_menu_1";
	this.main_menu_1.parent = this;
	this.main_menu_1.setTransform(125.55,319,1,1,0,0,0,86.5,19.9);
	new cjs.ButtonHelper(this.main_menu_1, 0, 1, 2, false, new lib.main_menu(), 3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.sect1_butt},{t:this.sect2_butt}]}).to({state:[]},1).to({state:[{t:this.main_menu_1}]},53).to({state:[]},1).to({state:[{t:this.main_menu_1}]},34).wait(1));

	// Text
	this.instance = new lib.CachedTexturedBitmap_1();
	this.instance.parent = this;
	this.instance.setTransform(179,11.05,0.5,0.5);

	this.instance_1 = new lib.CachedTexturedBitmap_2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(198,11.05,0.5,0.5);

	this.instance_2 = new lib.CachedTexturedBitmap_3();
	this.instance_2.parent = this;
	this.instance_2.setTransform(193,11.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},29).to({state:[{t:this.instance_2}]},30).wait(31));

	// Shapes
	this.instance_3 = new lib.star("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(88.75,96.55,1,1,0,0,0,57,54.5);
	this.instance_3._off = true;

	this.instance_4 = new lib.green_ball("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(83.55,208.5,1,1,0,0,0,54.5,54.5);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(29).to({_off:false},0).to({x:401.7,y:316.55},25).to({_off:true},1).wait(35));
	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(59).to({_off:false},0).to({x:456.5,y:197.5},30).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(303.6,211.1,207.89999999999998,160.50000000000003);
// library properties:
lib.properties = {
	id: 'B98DD005DCFB3E409F76597BA432E6D3',
	width: 550,
	height: 400,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/timeline_control_HTML5 Canvas_atlas_.png?1562340505966", id:"timeline_control_HTML5 Canvas_atlas_"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['B98DD005DCFB3E409F76597BA432E6D3'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;